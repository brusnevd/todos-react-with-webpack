import React from 'react';
import classes from "./btn-delete.module.css"
import { saveToLocalStorage } from "./../../../localstorage";

function deleteTasks(props) {
    if (!confirm("Sure?")) {
        return;
    }

    let mas;

    if (props.activeBtn === "All") {
        mas = [];
    }

    if (props.activeBtn === "Active") {
        mas = props.state.tasks.filter((current) => {
            return current.check === true;
        });
    }

    if (props.activeBtn === "Completed") {
        mas = props.state.tasks.filter((current) => {
            return current.check !== true;
        });
    }

    // saveToLocalStorage(props.id, mas);

    props.setState(
        {
            text: "",
            tasks: mas,
        }
    );

    props.setCount(0);
}

const BtnDelete = (props) => {
    let text = "";
    if (props.isActive.All === true || props.isActive.Active === true || props.isActive.Completed === true) {
        text = "Delete " + props.activeBtn + " tasks";
    }
    return (
        <div className={classes.btnDelete} onClick={() => deleteTasks(props)}>
            { text }
        </div>
    )
}

export default BtnDelete;