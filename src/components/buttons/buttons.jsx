import React from 'react';
import classes from "./buttons.module.css"
import BtnInfo from "./btn-info/BtnInfo.jsx";
import BtnDelete from "./btn-delete/BtnDelete.jsx";
import BtnsShowInfo from "./btns-show-info/BtnsShowInfo.jsx";

const Buttons = (props) => {
    return (
        <div className={classes.buttonsContainer}>
            <BtnInfo count={props.count} activeBtn={props.activeBtn} />
            <BtnsShowInfo state={props.state} count={props.count} setCount={props.setCount} activeBtn={props.activeBtn} setBtn={props.setBtn} isActive={props.isActive} setActive={props.setActive} />
            <BtnDelete state={props.state} setState={props.setState} count={props.count} setCount={props.setCount} activeBtn={props.activeBtn} setBtn={props.setBtn} id={props.id} isActive={props.isActive} />
        </div>
    )
}

export default Buttons;