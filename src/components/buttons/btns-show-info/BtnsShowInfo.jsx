import React from 'react';
import classes from "./btns-show.module.css"
import BtnAll from "./btn-all/BtnAll.jsx";
import BtnActive from "./btn-active/BtnActive.jsx";
import BtnCompleted from "./btn-completed/BtnCompleted.jsx";

const BtnsShowInfo = (props) => {
    return (
        <div className={classes.btnsShowInfo}>
            <BtnAll state={props.state} count={props.count} setCount={props.setCount} activeBtn={props.activeBtn} setBtn={props.setBtn} isActive={props.isActive} setActive={props.setActive} />
            <BtnActive state={props.state} count={props.count} setCount={props.setCount} activeBtn={props.activeBtn} setBtn={props.setBtn} isActive={props.isActive} setActive={props.setActive} />
            <BtnCompleted state={props.state} count={props.count} setCount={props.setCount} activeBtn={props.activeBtn} setBtn={props.setBtn} isActive={props.isActive} setActive={props.setActive} />
        </div>
    )
}

export default BtnsShowInfo;