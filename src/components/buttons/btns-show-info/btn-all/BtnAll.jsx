import React from 'react';

function allClickHandler(props) {
    if (props.isActive.All) {
        props.setActive({
            All: false,
            Active: false,
            Completed: false,
        })
        props.setBtn("0");
        props.setCount(0);
    } else {
        props.setActive({
            All: true,
            Active: false,
            Completed: false,
        })
        props.setBtn("All");
        props.setCount(props.state.tasks.length);
    }
}

const BtnAll = (props) => {
    return (
        <div onClick={() => allClickHandler(props)}>
            All
        </div>
    )
}

export default BtnAll;






// completed --------------------------------------------------------------------------
// let count = props.state.tasks.reduce((count, current) => count + !!current.check, 0);