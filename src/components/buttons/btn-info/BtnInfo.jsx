import React from 'react';
import classes from "./btn-info.css"

const BtnInfo = (props) => {
    return (
        <div className={classes.button}>
            {props.activeBtn} - <span className="number">{props.count}</span>
        </div>
    )
}

export default BtnInfo;