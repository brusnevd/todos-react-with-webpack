import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';

// function renderApp() {

let config = {
    id: 1,
}

ReactDOM.render(
    <App config={config} />,
    document.getElementById('root')
);

// }

// renderApp();

// export default renderApp;