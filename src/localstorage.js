export function saveToLocalStorage(id, obj) {
    localStorage.setItem(
        "list" + id,
        JSON.stringify(obj),
    );
}